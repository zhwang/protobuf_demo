#include "stdafx.h"
#include "prototest.h"
#include "plate.pb.h"
#include "handler.h"

extern handler* phandler = nullptr;

int set_plates_info(unsigned char* pPlatesInfoBuf, int length)
{
	if (phandler == nullptr) return -1;
	auto ret = phandler->set_plates_info_impl(pPlatesInfoBuf, length);
	return ret;
}

int get_plates_info(unsigned char** pPlatesInfoBuf, int* length)
{
	if (phandler == nullptr) return -1;
	auto ret = phandler->get_plates_info_impl(pPlatesInfoBuf, length);
	return ret;
}

int set_scan_info(unsigned char* pScanInfoBuf, int length)
{
	if (phandler == nullptr) return -1;
	auto ret = phandler->set_scan_info_impl(pScanInfoBuf, length);
	return ret;
}

int get_scan_info(unsigned char** pScanInfoBuf, int* length)
{
	if (phandler == nullptr) return -1;
	auto ret = phandler->get_scan_info_impl(pScanInfoBuf, length);
	return ret;

}

