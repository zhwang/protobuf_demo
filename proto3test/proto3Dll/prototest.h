#pragma once

#ifdef __cplusplus
#ifdef PROTO3DLL_EXPORTS
#define COMMANDLIB_API extern "C" __declspec( dllexport )
#else
#define COMMANDLIB_API extern "C" __declspec( dllimport )
#endif
#else
#define COMMANDLIB_API 
#endif

COMMANDLIB_API int set_plates_info(unsigned char* pPlatesInfoBuf, int length);

COMMANDLIB_API int get_plates_info(unsigned char** pPlatesInfoBuf, int* length);

COMMANDLIB_API int set_scan_info(unsigned char* pScanInfoBuf, int length);

COMMANDLIB_API int get_scan_info(unsigned char** pScanInfoBuf, int* length);
