#pragma once
class handler
{
public:
	handler();
	~handler();

	void* plateInfoBuffer;
	int plateInfoBufferSize;

	void* scanInfoBuffer;
	int scanInfoBufferSize;

	int set_plates_info_impl(unsigned char* pBuffer, int length);
	int get_plates_info_impl(unsigned char** pBuffer, int* length);

	int set_scan_info_impl(unsigned char* pBuffer, int length);
	int get_scan_info_impl(unsigned char** pBuffer, int* length);

};

