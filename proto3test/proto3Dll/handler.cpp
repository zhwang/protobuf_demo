#include "stdafx.h"
#include "public.pb.h"
#include "plate.pb.h"
#include "scan.pb.h"
#include "handler.h"

handler::handler()
{
	plateInfoBuffer = nullptr;
	plateInfoBufferSize = 0;

	scanInfoBuffer = nullptr;
	scanInfoBufferSize = 0;
}


handler::~handler()
{
	if (plateInfoBuffer != nullptr)
	{
		free(plateInfoBuffer);
		plateInfoBuffer = nullptr;
	}

	plateInfoBufferSize = 0;

	if (scanInfoBuffer != nullptr)
	{
		free(scanInfoBuffer);
		scanInfoBuffer = nullptr;
	}

	scanInfoBufferSize = 0;
}

int handler::set_plates_info_impl(unsigned char* pBuffer, int length)
{
	Plate plate;
	plate.ParseFromArray(pBuffer, length);

	return 66;
}

int handler::get_plates_info_impl(unsigned char** pBuffer, int* length)
{
	Plate myplate;
	myplate.set_id(1);
	myplate.set_name("Custom Carrier");
	myplate.set_width(5000.000);
	myplate.set_height(5000.000);
	myplate.set_physicalsize_x_unit(DistanceUnit::DISTANCE_MICROMETER);
	myplate.set_physicalsize_y_unit(DistanceUnit::DISTANCE_MICROMETER);
	myplate.set_rows(1);
	myplate.set_columns(1);

	Well* myWell = myplate.add_wells();
	myWell->set_plate_id(1);
	myWell->set_position_x(0.000);
	myWell->set_position_y(0.000);
	myWell->set_width(5000.000);
	myWell->set_height(5000.000);
	myWell->set_well_shape(Shape::SHAPE_RECTANGLE);
	myWell->set_row(0);
	myWell->set_column(0);

	
	auto arraySize = myplate.ByteSizeLong();
	*length = static_cast<int>(arraySize);

	if(arraySize > static_cast<size_t>(plateInfoBufferSize))
	{
		if(plateInfoBuffer != nullptr)
		{
			free(plateInfoBuffer);
		}
		plateInfoBuffer = malloc(arraySize);
		plateInfoBufferSize = arraySize;
	}

	myplate.SerializeToArray(plateInfoBuffer, arraySize);
	*pBuffer = static_cast<unsigned char*>(plateInfoBuffer);

	return 88;
}

int handler::set_scan_info_impl(unsigned char* pBuffer, int length)
{
	Scan scan;
	scan.ParseFromArray(pBuffer, length);
	//TODO: to use the scan object.

	return 66;

}

int handler::get_scan_info_impl(unsigned char** pBuffer, int* length)
{
	Scan myScan;
	myScan.set_id(11);
	myScan.set_plate_id(121);
	myScan.set_name("Test Scan");
	myScan.set_physical_size_x(0.123);
	myScan.set_physical_size_y(0.234);
	myScan.set_physical_size_z(0.345);
	myScan.set_physical_uint_x(DistanceUnit::DISTANCE_MICROMETER);
	myScan.set_physical_uint_y(DistanceUnit::DISTANCE_MICROMETER);
	myScan.set_physical_uint_z(DistanceUnit::DISTANCE_MICROMETER);
	myScan.set_time_increment(10);
	myScan.set_time_increment_unit(TimeUnit::TIME_MILISECOND);
	myScan.set_dimension_order("XYZCT");
	myScan.set_tile_width(90);
	myScan.set_tile_height(190);
	myScan.set_data_type("uint16");
	myScan.set_significant_bits(14);

	auto channel_a = myScan.add_channel_define();
	channel_a->set_id(1);
	channel_a->set_name("Red");

	auto channel_b = myScan.add_channel_define();
	channel_b->set_id(2);
	channel_b->set_name("Green");

	auto channel_c = myScan.add_channel_define();
	channel_c->set_id(3);
	channel_c->set_name("Blue");

	auto scan_region_a = myScan.add_region_list();
	scan_region_a->set_id(1);
	scan_region_a->set_size_x(100.0);
	scan_region_a->set_size_y(200.0);
	scan_region_a->set_size_z(300.0);
	scan_region_a->set_size_time(1.2345);

	auto well_sample = scan_region_a->add_sample_list();
	well_sample->set_id(1);
	well_sample->set_plate_id(1);
	well_sample->set_position_x(0.000);
	well_sample->set_position_y(0.000);
	well_sample->set_position_z(0.200);
	well_sample->set_scan_id(1);
	well_sample->set_region_id(11);
	well_sample->set_image_id(112);
	well_sample->set_size_x(1.234);
	well_sample->set_size_y(1.234);
	well_sample->set_size_z(1.234);
	well_sample->set_physicalsize_x_unit(DistanceUnit::DISTANCE_MILIMETER);
	well_sample->set_physicalsize_y_unit(DistanceUnit::DISTANCE_MILIMETER);
	well_sample->set_physicalsize_z_unit(DistanceUnit::DISTANCE_MILIMETER);


	auto arraySize = myScan.ByteSizeLong();
	*length = static_cast<int>(arraySize);

	if (arraySize > static_cast<size_t>(scanInfoBufferSize))
	{
		if (scanInfoBuffer != nullptr)
		{
			free(scanInfoBuffer);
		}
		scanInfoBuffer = malloc(arraySize);
		scanInfoBufferSize = arraySize;
	}

	myScan.SerializeToArray(scanInfoBuffer, arraySize);
	*pBuffer = static_cast<unsigned char*>(scanInfoBuffer);

	return 88;
}

