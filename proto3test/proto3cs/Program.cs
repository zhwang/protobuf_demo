﻿using System;
using System.Runtime.InteropServices;
using Google.Protobuf;
using ThorLabs.ImageStoreLibrary.Protobuf.Plate;
using ThorLabs.ImageStoreLibrary.Protobuf.Public;
using ThorLabs.ImageStoreLibrary.Protobuf.Scan;

namespace proto3cs
{
    static class FunctionProvider
    {
        const string dllPath = @"proto3Dll.dll";

        [DllImport(dllPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int set_plates_info(IntPtr buffer, int length);

        [DllImport(dllPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int get_plates_info(ref IntPtr buffer,ref int length);

        [DllImport(dllPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int set_scan_info(IntPtr buffer, int length);

        [DllImport(dllPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int get_scan_info(ref IntPtr buffer, ref int length);

    }

    class Program
    {
        private static Plate GetPlateInfo()
        {
            var pbuf = IntPtr.Zero;
            var bufLen = 0;
            var retGet = FunctionProvider.get_plates_info(ref pbuf, ref bufLen);

            byte[] bufArray = new byte[bufLen];
            Marshal.Copy(pbuf, bufArray, 0, bufLen);
            
            return Plate.Parser.ParseFrom(bufArray);
        }

        private static void SetPlateInfo(Plate p)
        {
            var barray = p.ToByteArray();
            var barLen = barray.Length;

            var pinnedArray = GCHandle.Alloc(barray, GCHandleType.Pinned);
            var pointer = pinnedArray.AddrOfPinnedObject();
            var retSet = FunctionProvider.set_plates_info(pointer, barLen);
            pinnedArray.Free();
        }

        private static Scan GetScanInfo()
        {
            var pbuf = IntPtr.Zero;
            var bufLen = 0;
            var retGet = FunctionProvider.get_scan_info(ref pbuf, ref bufLen);

            byte[] bufArray = new byte[bufLen];
            Marshal.Copy(pbuf, bufArray, 0, bufLen);

            return Scan.Parser.ParseFrom(bufArray);
        }

        private static void SetScanInfo(Scan s)
        {
            var barray = s.ToByteArray();
            var barLen = barray.Length;

            var pinnedArray = GCHandle.Alloc(barray, GCHandleType.Pinned);
            var pointer = pinnedArray.AddrOfPinnedObject();
            var retSet = FunctionProvider.set_scan_info(pointer, barLen);
            pinnedArray.Free();
        }



        static void Main(string[] args)
        {
            var p = GetPlateInfo();
            Console.WriteLine(p.ToString());

            var plate = new Plate
            {
                Name = "testPlate",
                Rows = 1,
                Columns = 2,
                Width = 99,
                Height = 88,
                PhysicalsizeXUnit = DistanceUnit.DistanceMicrometer,
                PhysicalsizeYUnit = DistanceUnit.DistanceMicrometer,
            };

            plate.Wells.Add(new Well(){});

            SetPlateInfo(plate);



            var scan = GetScanInfo();
            scan.Name = "Thorlabs Test Scan";

            Console.WriteLine(scan.ToString());

            SetScanInfo(scan);

            Console.ReadKey();

        }
    }






}
